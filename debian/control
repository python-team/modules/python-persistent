Source: python-persistent
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 11),
               dh-python,
               python3-all-dev,
               python3-repoze.sphinx.autointerface,
               python3-setuptools,
               python3-sphinx,
               python3-zope.interface,
               python3-cffi,
               python3-manuel,
               python3-sphinx,
               python3-sphinx-rtd-theme,
               python3-zope.deferredimport,
Standards-Version: 4.7.0
Homepage: https://github.com/zopefoundation/persistent/
Vcs-Git: https://salsa.debian.org/python-team/packages/python-persistent.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-persistent
Rules-Requires-Root: no

Package: python3-persistent
Architecture: any
Depends: ${misc:Depends},
  ${python3:Depends},
  ${shlibs:Depends},
  python3-cffi,
Suggests: python-persistent-doc
Description: Automatic persistence for Python objects
 This package contains a generic persistence implementation for Python. It
 forms the core protocol for making objects interact "transparently" with
 a database such as the ZODB.
 .
 This is the Python 3 version.

Package: python-persistent-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Multi-Arch: foreign
Description: Automatic persistence for Python objects - documentation
 This package contains a generic persistence implementation for Python. It
 forms the core protocol for making objects interact "transparently" with
 a database such as the ZODB.
 .
 This package contains the Python module documentation. Alternatively,
 there is an online version at https://persistent.readthedocs.io/
